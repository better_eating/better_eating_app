import 'package:better_eating_app/bloc/session/sessionEvents.dart';
import 'package:better_eating_app/bloc/session/sessionStates.dart';
import 'package:better_eating_app/bloc/symptom/SymptomBlock.dart';
import 'package:better_eating_app/pages/HomePage.dart';
import 'package:better_eating_app/pages/LoginPage.dart';
import 'package:better_eating_app/repository/ApiRespository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;

import 'bloc/session/sessionBloc.dart';

void main() {
  final http.Client httpClient = http.Client();

  BlocOverrides.runZoned(
    () => runApp(MultiRepositoryProvider(
      providers: [
        RepositoryProvider<ApiRepository>(
          create: (context) => ApiRepository(
            httpClient: httpClient,
          ),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (BuildContext context) {
              return SymptomBloc(
                  apiRepository: RepositoryProvider.of<ApiRepository>(context));
            },
          ),
          BlocProvider(
            create: (BuildContext context) {
              return SessionBloc(
                apiRepository: RepositoryProvider.of<ApiRepository>(context),
                symptomBloc: BlocProvider.of<SymptomBloc>(context),
              )..add(SessionEventLoginFromMemory());
            },
          ),
        ],
        child: const App(),
      ),
    )),
    blocObserver: AppBlocObserver(),
  );
}

/// Custom [BlocObserver] that observes all bloc and cubit state changes.
class AppBlocObserver extends BlocObserver {
  @override
  void onChange(BlocBase bloc, Change change) {
    super.onChange(bloc, change);
    if (bloc is Cubit) print(change);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }
}

class App extends StatelessWidget {
  /// {@macro app}
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppView();
  }
}

class AppView extends StatelessWidget {
  /// {@macro app_view}
  const AppView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: BlocBuilder<SessionBloc, SessionState>(builder: (context, state) {
        Widget returnWidget = Container();
        if (state is SessionStateLoggedOut) {
          returnWidget = LoginPage();
        }
        if (state is SessionStateLoggedIn) {
          returnWidget = HomePage();
        }
        return returnWidget;
      }),
    );
  }
}
