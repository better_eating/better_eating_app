class LoginAnswer{
  final List<dynamic> errors;
  final String token;
  final String key;
  final List<dynamic> passwordErrors;
  final List<dynamic> usernameErrors;
  final List<dynamic> new_password1Errors;
  final List<dynamic> new_password2Errors;



  LoginAnswer(this.errors,this.passwordErrors,this.new_password1Errors, this.new_password2Errors, this.usernameErrors, this.token, this.key);

  LoginAnswer.fromJson(Map<String, dynamic> json)
      : errors = json['non_field_errors'],
        passwordErrors = json['password1'],
        usernameErrors = json['username'],
        new_password1Errors = json['new_password1'],
        new_password2Errors = json['new_password2'],
        token = json['token'],
        key = json['key'];

  Map<String, dynamic> toJson() =>
      {
        'errors': errors,
        'token': token,
        'key': key,
      };

  @override
  String toString() {
    return 'LoginAnswer{errors: $errors, token: $token, key: $key, passwordErrors: $passwordErrors, usernameErrors: $usernameErrors}';
  }


}