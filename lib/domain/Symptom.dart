class Symptom {
  final int? id;
  final String name;
  final int? intensity;

  Symptom({this.id, required this.name, this.intensity});

  Symptom.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        name = json["name"],
        intensity = json["intensity"];

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'intensity': intensity,
      };

  @override
  String toString() {
    return 'Symptom{id: $id, name: $name, intensity: $intensity}';
  }

  String getIntensity() {
    if (intensity != null) {
      return "$intensity";
    } else {
      return "";
    }
  }
}
