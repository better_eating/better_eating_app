import 'dart:ui';

import 'package:better_eating_app/bloc/symptom/SymptomBlock.dart';
import 'package:better_eating_app/bloc/symptom/SymptomState.dart';
import 'package:better_eating_app/pages/SymptomPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/session/sessionBloc.dart';
import '../bloc/session/sessionEvents.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Better Eating"),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.green,
              ),
              child: Text(
                'Better Eating',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.sick),
              title: Text('Symptoms'),
              trailing: BlocBuilder<SymptomBloc, SymptomState>(
                builder: (context, state) {
                  Widget returnWidget = Container();

                  if (state is SymptomStateData) {
                    returnWidget = Text("${state.symptoms.length}");
                  } else if (state is SymptomStateErrors) {
                    returnWidget = Icon(Icons.error);
                  } else if (state is SymptomStateLoading) {
                    returnWidget = CircularProgressIndicator();
                  }
                  return returnWidget;
                },
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (_) {
                    return SymptomPage();
                  }),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.question_answer),
              title: Text('Ingredients'),
            ),
            ListTile(
              leading: Icon(Icons.fastfood),
              title: Text('Meals'),
            ),
            ElevatedButton(
              child: const Text('Logout'),
              onPressed: () {
                BlocProvider.of<SessionBloc>(context).add(SessionEventLogout());
              },
            )
          ],
        ),
      ),
      body: Center(
        child: Text("Body"),
      ),
    );
  }
}
