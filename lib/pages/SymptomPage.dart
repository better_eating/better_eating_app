import 'package:better_eating_app/bloc/symptom/SymptomBlock.dart';
import 'package:better_eating_app/bloc/symptom/SymptomEvent.dart';
import 'package:better_eating_app/bloc/symptom/SymptomState.dart';
import 'package:better_eating_app/topics/symptoms/SymptomsListWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SymptomPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Symptoms")),
      body: BlocBuilder<SymptomBloc, SymptomState>(
        builder: (context, state) {
          Widget returnWidget = Container();

          if (state is SymptomStateLoading) {
            returnWidget = Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is SymptomStateErrors) {
            returnWidget = Center(
              child: Icon(Icons.error),
            );
          } else if (state is SymptomStateData) {
            returnWidget = SymptomsListWidget(
              symptoms: state.symptoms,
            );
          }
          return returnWidget;
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        onPressed: () {
          BlocProvider.of<SymptomBloc>(context).add(SymptomEventRefresh());
        },
      ),
    );
  }
}
