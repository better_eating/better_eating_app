import 'package:better_eating_app/domain/LoginAnswer.dart';
import 'package:better_eating_app/domain/Symptom.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert' as convert;

import 'package:shared_preferences/shared_preferences.dart';

class ApiRepository {
  final http.Client httpClient;
  final String preSavedDefaultUrl = "https://bettereating.node1337.de";

  ApiRepository({
    required this.httpClient,
  });

  Future<String> authenticate({
    required String url,
    required String username,
    required String password,
  }) async {
    if ("$url" == "") {
      url = preSavedDefaultUrl;
    }

    var innerUrl = '$url/api/api-token-auth/';
    var response = await http.post(
      Uri.parse(innerUrl),
      body: {'username': username, 'password': password},
    );

    if (response.statusCode != 200) {
      throw Exception(response.body);
    }

    var jsonResponse = convert.jsonDecode(response.body);
    print(jsonResponse);
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString('token', jsonResponse["token"]);
    sharedPreferences.setString('username', username);
    sharedPreferences.setString('url', url);
    return jsonResponse["token"];
  }

  Future<String> getPresavedUrl() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? prefUrl = sharedPreferences.getString('url');
    if (prefUrl != null) {
      return prefUrl;
    } else {
      return preSavedDefaultUrl;
    }
  }

  Future<bool> hasToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? prefToken = sharedPreferences.getString('token');
    return prefToken != null;
  }

  Future<String> getToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? prefToken = sharedPreferences.getString('token');
    if (prefToken != null) {
      return prefToken;
    } else {
      return "";
    }
  }

  Future<void> deleteToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? url = sharedPreferences.getString("url");
    if ("$url" == "" || url == null) {
      url = preSavedDefaultUrl;
    }

    var innerUrl = '$url/rest-auth/logout/';
    var response = await http.post(
      Uri.parse(innerUrl),
      headers: {
        "Authorization": "Token ${sharedPreferences.getString("token")}"
      },
    );
    sharedPreferences.remove("token");
    sharedPreferences.remove('username');
  }

  Future<List<Symptom>> getSymptoms() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? url = sharedPreferences.getString("url");
    if ("$url" == "" || url == null) {
      url = preSavedDefaultUrl;
    }

    var innerUrl = '$url/api/symptoms';
    var response = await http.get(
      Uri.parse(innerUrl),
      headers: {
        "Authorization": "Token ${sharedPreferences.getString("token")}"
      },
    );
    var jsonResponse = convert.jsonDecode(response.body);
    List<Symptom> symptoms = [];
    jsonResponse.forEach((element) {
      Symptom symptom = Symptom.fromJson(element);
      symptoms.add(symptom);
    });
    return symptoms;
  }
}
