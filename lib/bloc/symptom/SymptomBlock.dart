import 'package:better_eating_app/bloc/symptom/SymptomEvent.dart';
import 'package:better_eating_app/bloc/symptom/SymptomState.dart';
import 'package:better_eating_app/domain/Symptom.dart';
import 'package:better_eating_app/repository/ApiRespository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SymptomBloc extends Bloc<SymptomEvent, SymptomState> {
  final ApiRepository apiRepository;

  SymptomBloc({required this.apiRepository})
      : super(SymptomStateData(symptoms: [])) {
    on<SymptomEventAdd>((event, emit) {
      // perform add call

      // get data
      add(SymptomEventRefresh());
    });
    on<SymptomEventUpdate>((event, emit) {
      // get data
      add(SymptomEventRefresh());
    });
    on<SymptomEventDelete>((event, emit) {
      // get data
      add(SymptomEventRefresh());
    });
    on<SymptomEventRefresh>((event, emit) async {
      // get data
      // todo try catch and emit errors
      List<Symptom> symptoms = await apiRepository.getSymptoms();
      emit(SymptomStateData(symptoms: symptoms));
    });
  }
}
