import 'package:better_eating_app/domain/Symptom.dart';
import 'package:equatable/equatable.dart';

abstract class SymptomState extends Equatable {
  const SymptomState();

  @override
  List<Object> get props => [];
}

class SymptomStateLoading extends SymptomState {
  // some api requests are in progress

  SymptomStateLoading();

  @override
  List<Object> get props => [];
}

class SymptomStateData extends SymptomState {
  // symptoms have been fetched from the api and can be displayed
  List<Symptom> symptoms;

  SymptomStateData({required this.symptoms});

  @override
  List<Object> get props => [symptoms];
}

class SymptomStateErrors extends SymptomState {
  // Errors have occured and they should be displayed

  List<String> errors;

  SymptomStateErrors({required this.errors});

  @override
  List<Object> get props => [errors];
}
