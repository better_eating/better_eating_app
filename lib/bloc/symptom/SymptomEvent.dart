import 'package:better_eating_app/domain/Symptom.dart';
import 'package:equatable/equatable.dart';

abstract class SymptomEvent extends Equatable {
  const SymptomEvent();

  @override
  List<Object> get props => [];
}

class SymptomEventRefresh extends SymptomEvent {
  // user wants to refresh the symptoms
}

class SymptomEventAdd extends SymptomEvent {
  // user wants to add a new symptom
  Symptom symptom;

  SymptomEventAdd({required this.symptom});

  @override
  List<Object> get props => [symptom];
}



class SymptomEventDelete extends SymptomEvent {
  // user wants to delete a symptom
  Symptom symptom;

  SymptomEventDelete({required this.symptom});

  @override
  List<Object> get props => [symptom];
}

class SymptomEventUpdate extends SymptomEvent {
  // user wants to Update a symptom
  Symptom symptom;

  SymptomEventUpdate({required this.symptom});

  @override
  List<Object> get props => [symptom];
}
