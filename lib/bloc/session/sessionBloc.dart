import 'package:better_eating_app/bloc/session/sessionEvents.dart';
import 'package:better_eating_app/bloc/session/sessionStates.dart';
import 'package:better_eating_app/bloc/symptom/SymptomBlock.dart';
import 'package:better_eating_app/bloc/symptom/SymptomEvent.dart';
import 'package:better_eating_app/repository/ApiRespository.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SessionBloc extends Bloc<SessionEvent, SessionState> {
  final ApiRepository apiRepository;

  final SymptomBloc symptomBloc;

  SessionBloc({required this.apiRepository, required this.symptomBloc})
      : super(SessionStateLoggedOut()) {
    on<SessionEventLogin>((event, emit) async {
      // perform login action
      // set token in storage
      String token = await apiRepository.authenticate(
        url: event.url,
        username: event.username,
        password: event.password,
      );

      emit(SessionStateLoggedIn(token: token));

      symptomBloc.add(SymptomEventRefresh());
    });
    on<SessionEventLogout>((event, emit) {
      // perform delete action
      // remove token from storage

      emit(SessionStateLoggedOut());
    });
    on<SessionEventLoginFromMemory>((event, emit) async {
      // try to log in with a pre - saved token
      bool hasToken = await apiRepository.hasToken();

      if (hasToken) {
        String token = await apiRepository.getToken();
        emit(SessionStateLoggedIn(token: token));

        // load initial data after login
        symptomBloc.add(SymptomEventRefresh());
      } else {
        emit(SessionStateLoggedOut());
      }
    });
  }
}
