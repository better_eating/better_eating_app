import 'package:equatable/equatable.dart';

abstract class SessionEvent extends Equatable {}

class SessionEventLogin extends SessionEvent {
  // user is trying to login with the provided credentials
  String url;
  String username;
  String password;

  SessionEventLogin(
      {required this.url, required this.username, required this.password});

  @override
  List<Object?> get props => [url, username, password];
}

class SessionEventLoginFromMemory extends SessionEvent{
  // user is trying to log in from SharedPrefrences memory

  @override
  List<Object?> get props => [];

}

class SessionEventLogout extends SessionEvent {
  // user wants to logout
  @override
  List<Object?> get props => [];
}
