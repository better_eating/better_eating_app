import 'package:equatable/equatable.dart';

abstract class SessionState extends Equatable {}

class SessionStateLoggedIn extends SessionState {
  // user is logged in
  String token;

  SessionStateLoggedIn({required this.token});

  @override
  List<Object?> get props => [token];
}

class SessionStateLoggedOut extends SessionState {
  // user is not logged in
  @override
  List<Object?> get props => [];
}
