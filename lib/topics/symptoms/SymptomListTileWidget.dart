import 'package:better_eating_app/domain/Symptom.dart';
import 'package:flutter/material.dart';

class SymptomListTileWidget extends StatelessWidget {
  Symptom symptom;

  SymptomListTileWidget({required this.symptom});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        //leading: Text("${symptom.id}"),
        title: Text("${symptom.name} ${symptom.getIntensity()}"),
        trailing: Icon(Icons.chevron_right),
      ),
    );
  }
}
