import 'package:better_eating_app/domain/Symptom.dart';
import 'package:better_eating_app/topics/symptoms/SymptomListTileWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SymptomsListWidget extends StatelessWidget {
  List<Symptom> symptoms;

  SymptomsListWidget({required this.symptoms});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: symptoms.length,
      itemBuilder: ((context, i) {
        return SymptomListTileWidget(symptom: symptoms[i]);
      }),
    );
  }
}
